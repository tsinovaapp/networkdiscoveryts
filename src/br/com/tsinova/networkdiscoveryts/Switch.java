package br.com.tsinova.networkdiscoveryts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Switch {

    private String ip;
    private String mac;
    private List<Port> ports;
    private String description;

    public Switch(Device device){
        this.ip = device.getIp();
        this.mac = device.getMac();
        this.ports = new ArrayList<>();
        this.description = device.getDescription();
    }
    
    public Switch(String ip) {
        this.ip = ip;
        this.ports = new ArrayList<>();
        this.description = "";
        this.mac = "";
    }

    public Switch(String ip, String mac) {
        this.ip = ip;
        this.mac = mac;
        this.ports = new ArrayList<>();
        this.description = "";
    }

    public Switch(String ip, String mac, String description) {
        this.ip = ip;
        this.mac = mac;
        this.description = description;
        this.ports = new ArrayList<>();
    }        

    public Switch() {
        this.ports = new ArrayList<>();
        this.ip = "";
        this.mac = "";
        this.description = "";
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public List<Port> getPorts() {
        return ports;
    }

    public void setPorts(List<Port> ports) {
        this.ports = ports;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Add uma porta sem dispositivo conectado
     * @param port 
     */
    public void addPort(Port port) {
        if (ports == null) {
            this.ports = new ArrayList<>();
        }
        this.ports.add(port);
    }
    
    /**
     * Add uma porta com um dispositivo já
     * @param port
     * @param device 
     */
    public void addPort(Port port, Device device) {
        if (ports == null) {
            this.ports = new ArrayList<>();
        }
        
        // descarta portas desnecessárias
        if (port.getPort() > getNumberPortsByDescription() || port.getPort() < 1){
            return;
        }
        
        this.ports.add(port);
        port.addDevice(new Device(port, device));
    }
    
    /**
     * Add dispositivo no switch
     * @param portNumber
     * @param device 
     */
    public void addDevice(int portNumber, Device device){        
        for(Port port : ports){
            if (port.getPort() == portNumber){
                port.addDevice(new Device(port, device));
                return;
            }
        }        
        addPort(new Port(this, portNumber), device);        
    }
    
    /**
     * Retorna a porta do switch na qual o dispositivo está conectado
     * @param device
     * @param swichs
     * @return 
     */
    public Port connectedDevice(Device device, List<Switch> swichs){
        for(Port port : ports){            
            if (port.connectedDevice(device, swichs)){
                return port;                
            }
        }
        return null;
        
    }
    
    /**
     * Retorna a porta em que o dispositivo está conectado
     * @param device
     * @return 
     */
    public Port containsDevice(Device device){
        for(Port port : ports){
            if (port.containsDevice(device)){
                return port;
            }
        }
        return null;
    }
    
    /**
     * Retorna todos os dispositivos conectados no switch
     * @return 
     */
    public Map<String, Device> getConnectedDevices(){
        Map<String, Device> map = new HashMap<>();        
        for(Port port : ports){            
            for(Device device : port.getDevices()){
                map.put(device.getMac(), device);
            }            
        }
        return map;
    }
    
    /**
     * Retorna o número de portas de um switch com base na descrição
     * @param description
     * @return 
     */
    private int getNumberPortsByDescription(){
        
        if (description == null || description.isEmpty()){
            return 0;
        }
        
        description = description.trim();
        
        String text[] = description.split(" ");       
        String regexs[] = new String[]{"(\\d+)-Port", "(\\d+)G", "(\\d+)GB", "(\\d+)P", "(\\d+)Port"};

        for (String regex : regexs) {
            for (String partText : text) {
                partText = partText.trim();
                if (partText.matches(regex)) {
                   return Integer.parseInt(partText.replaceAll(regex, "$1"));
                }
            }

        }
        
        return 0;
        
    }

}
