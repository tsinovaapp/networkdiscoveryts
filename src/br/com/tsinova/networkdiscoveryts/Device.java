package br.com.tsinova.networkdiscoveryts;

public class Device {
    
    private String ip;
    private String mac;
    private String type;
    private String category;
    private String systems;
    private String os;
    private String vendor;
    private String description;
    private Port port;

    public Device(Port port, String ip) {
        this.port = port;
        this.ip = ip;
    }

    public Device(Port port, String ip, String mac) {
        this.port = port;
        this.ip = ip;
        this.mac = mac;
    }

    public Device(Port port, String ip, String mac, String type, 
            String category, String systems, String os, String description) {
        this.port = port;
        this.ip = ip;
        this.mac = mac;
        this.type = type;
        this.category = category;
        this.systems = systems;
        this.os = os;
        this.description = description;
    }

    public Device() {
    }
    
    public Device(Device device){
        this.port = device.getPort();
        this.ip = device.getIp();
        this.mac = device.getMac();
        this.type = device.getType();
        this.category = device.getCategory();
        this.systems = device.getSystems();
        this.os = device.getOs();
        this.vendor = device.getVendor();
        this.description = device.getDescription();
    }
    
    public Device(Port port, Device device){
        this.port = port;
        this.ip = device.getIp();
        this.mac = device.getMac();
        this.type = device.getType();
        this.category = device.getCategory();
        this.systems = device.getSystems();
        this.os = device.getOs();
        this.vendor = device.getVendor();
        this.description = device.getDescription();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSystems() {
        return systems;
    }

    public void setSystems(String systems) {
        this.systems = systems;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    public Port getPort() {
        return port;
    }

    public void setPort(Port port) {
        this.port = port;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }    
    
    public boolean isSwitch() {
        return category != null && category.equalsIgnoreCase("switch");
    }

    @Override
    public String toString() {
        return "Device{" + "ip=" + ip + ", mac=" + mac + ", category=" + category + ", systems=" + systems + ", os=" + os + ", vendor=" + vendor + ", description=" + description + '}';
    }

}
