package br.com.tsinova.networkdiscoveryts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Port {

    private Switch swt;
    private int port;
    private List<Device> devices;

    public Port(Switch swt) {
        this.swt = swt;
        this.devices = new ArrayList<>();
    }

    public Port(Switch swt, List<Device> devices) {
        this.swt = swt;
        this.devices = devices;
    }

    public Port(Switch swt, int port, Device device) {
        this.swt = swt;
        this.port = port;
        this.devices = new ArrayList<>(Arrays.asList(device));
    }

    public Port(Switch swt, int port) {
        this.swt = swt;
        this.port = port;
        this.devices = new ArrayList<>();
    }

    public Port(Switch swt, int port, List<Device> devices) {
        this.swt = swt;
        this.port = port;
        this.devices = devices;
    }

    public Switch getSwitch() {
        return swt;
    }

    public void setSwitch(Switch swt) {
        this.swt = swt;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public void addDevice(Device device) {
        if (this.devices == null) {
            this.devices = new ArrayList<>();
        }
        device.setPort(this);
        this.devices.add(device);
    }

    public List<Device> getSwitchsConnected() {
        List<Device> switchs = new ArrayList<>();
        for (Device device : devices) {
            if (device.isSwitch()) {
                switchs.add(device);
            }
        }
        return switchs;

    }

    /**
     * Retorna true caso o dispositivo encontra-se conectado fisicamente na
     * porta
     *
     * @param dev
     * @param swichs
     * @return
     */
    public boolean connectedDevice(Device dev, List<Switch> swichs) {
        
        if (!containsDevice(dev)) {
            return false;
        }
        
        if (dev.isSwitch()) {

            if (!moreOneSwitchConnected()) {
                return true;
            }

            List<Device> othersSwitchs = getSwitchsConnected();

            boolean flag = false;
            
            for (Device device : othersSwitchs) {
                
                if (device.getIp().equalsIgnoreCase(dev.getIp())) {
                    continue;
                }
                
                flag = false;

                for (Switch swt : swichs) {
                    if (swt.getIp().equalsIgnoreCase(getSwitch().getIp())) {
                        continue;
                    }                   
                    Port port = swt.connectedDevice(device, swichs);
                    flag = port != null;
                    
                    if (flag){
                        break;
                    }
                    
                }
                
                if (!flag){
                    break;
                }

            }
            
            return flag;
            
        }
        
        return uniqueConnectedDevice();
    }

    /**
     * Retorna true caso a porta contenha o dispositivo
     *
     * @param dev
     * @return
     */
    public boolean containsDevice(Device dev) {
        for (Device device : devices) {
            if (device.getIp().equalsIgnoreCase(dev.getIp())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna se a porta possui algum switch conectado
     *
     * @return
     */
    public boolean noConnectedDevices() {
        return this.devices.isEmpty();
    }

    /**
     * Retorna se a porta possui ao menos um switch conectado
     *
     * @return
     */
    public boolean containsSwitchConnected() {
        for (Device device : devices) {
            if (device.isSwitch()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna true caso a porta possua mais de um switch conectado
     *
     * @return
     */
    public boolean moreOneSwitchConnected() {
        int i = 0;
        for (Device device : devices) {
            if (device.isSwitch()) {
                i++;
            }
        }
        return i > 1;
    }

    /**
     * Retorna se a porta é Trunk, ou seja, ela é uma ponte para uma nova sub
     * rede, ou seja, possui um switch conectado e nesse switch tem outros
     * dispositivos
     *
     * @return
     */
    public boolean isTrunk() {
        return containsSwitchConnected();
    }

    /**
     * Retorna true caso a porta só tenha um único dispositivo conectado
     *
     * @return
     */
    public boolean uniqueConnectedDevice() {
        return devices.size() == 1;
    }

}
