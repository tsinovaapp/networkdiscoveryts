package br.com.tsinova.networkdiscoveryts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

public class ReadSend extends Thread {

    private final List<Switch> switchsByParams;
    private final List<Community> communitiesbyParams;
    private final List<Output> listOutputByParams;
    private final Beat beatByParams;

    private boolean run;
    //private final Map<String, Device> networkDevicesByTableARP;

    public ReadSend(List<Switch> switchs, List<Community> communities, List<Output> listOutput, Beat beat) {
        this.switchsByParams = switchs;
        this.communitiesbyParams = communities;
        this.listOutputByParams = listOutput;
        this.beatByParams = beat;
        this.run = true;
        //this.networkDevicesByTableARP = new HashMap<>();
    }

    public void close() {
        this.run = false;
    }

    private CommunityTarget getTarget(String ipDevice, Community host) {
        CommunityTarget target = new CommunityTarget();
        if (host.getVersion().equals("2c")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + ipDevice + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version2c);
        } else if (host.getVersion().equals("1")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + ipDevice + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version1);
        }
        return target;
    }

    private CommunityTarget getTarget(String ipDevice, Community host, int timeOut, int retries) {
        CommunityTarget target = new CommunityTarget();
        if (host.getVersion().equals("2c")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + ipDevice + "/" + host.getPort()));
            target.setRetries(retries);
            target.setTimeout(timeOut);
            target.setVersion(SnmpConstants.version2c);
        } else if (host.getVersion().equals("1")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + ipDevice + "/" + host.getPort()));
            target.setRetries(retries);
            target.setTimeout(timeOut);
            target.setVersion(SnmpConstants.version1);
        }
        return target;
    }

    /**
     * Converte o valor armazenado em um OID em um tipo específico
     *
     * @param type
     * @param variableBinding
     * @return
     * @throws Exception
     */
    private Object getValueOID(int type, VariableBinding variableBinding) throws Exception {
        switch (type) {
            case 1:
                // inteiro
                return variableBinding.getVariable().toInt();
            case 2:
                // texto
                return variableBinding.getVariable().toString();
            case 4:
                // Long
                return variableBinding.getVariable().toLong();
            default:
                break;
        }
        throw new Exception("Invalid variable type");
    }

    private boolean existsMibOnDevice(String ipDevice, Community snmpCommunity, String mib) {

        boolean existsMib = false;

        try {

            CommunityTarget target = getTarget(ipDevice, snmpCommunity, 800, 1);

            TransportMapping transport = new DefaultUdpTransportMapping();
            Snmp snmp = new Snmp(transport);
            transport.listen();

            DefaultPDUFactory defaultPDUFactory = new DefaultPDUFactory(PDU.GETBULK);

            TreeUtils treeUtils = new TreeUtils(snmp, defaultPDUFactory);
            treeUtils.setIgnoreLexicographicOrder(!treeUtils.isIgnoreLexicographicOrder());

            List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + mib));

            for (TreeEvent event : events) {
                if (event == null) {
                    continue;
                }
                if (event.isError()) {
                    continue;
                }
                VariableBinding[] varBindings = event.getVariableBindings();
                for (VariableBinding varBinding : varBindings) {
                    if (varBinding == null) {
                        continue;
                    }
                    existsMib = true;
                    break;
                }
                if (existsMib) {
                    break;
                }
            }

            snmp.close();
            transport.close();

        } catch (Exception ex) {
            existsMib = false;

        }

        return existsMib;

    }

    /**
     * Verifica se o dispositivo é um nobreak
     *
     * @param ipDevice
     * @param snmpCommunities
     * @return
     */
    private boolean isNobreak(String ipDevice, List<Community> snmpCommunities) {

        boolean isNobreak = false;

        for (Community snmpCommunity : snmpCommunities) {
            isNobreak = existsMibOnDevice(ipDevice, snmpCommunity, "1.3.6.1.2.1.33");
            if (isNobreak) {
                break;
            }
        }

        return isNobreak;

    }

    /**
     * Verifica se o dispositivo é uma impressora
     *
     * @param ipDevice
     * @param snmpCommunities
     * @return
     */
    private boolean isPrinter(String ipDevice, List<Community> snmpCommunities) {

        boolean isPrinter = false;

        for (Community snmpCommunity : snmpCommunities) {
            isPrinter = existsMibOnDevice(ipDevice, snmpCommunity, "1.3.6.1.2.1.43");
            if (isPrinter) {
                break;
            }
        }

        return isPrinter;

    }

    /**
     * Verifica se o dispositivo é um switch
     *
     * @param ipDevice
     * @param snmpCommunities
     * @return
     */
    private boolean isSwitch(String ipDevice, List<Community> snmpCommunities) {

        boolean isSwitch = false;

        String service = getSysServices(ipDevice, snmpCommunities);

        if (service != null && !service.isEmpty()) {

            service = Integer.toBinaryString(Integer.parseInt(service));
            service = new StringBuilder(service).reverse().toString();

            if (service.startsWith("1", 1) || service.startsWith("1", 2)) {
                isSwitch = true;
            }

        }

        return isSwitch;

    }

    /**
     * Verifica se o dispositivo é um servidor
     *
     * @param ipDevice
     * @return
     */
    private boolean isServer(String ipDevice) {

        boolean isServer = false;

        try {

            String url = "https://" + ipDevice + "/redfish/v1/";
            HttpResponse<JsonNode> response = Unirest.get(url).asJson();

            if (response.getBody().getObject().has("Systems")) {
                isServer = true;
            }

        } catch (Exception ex) {
            isServer = false;
        }

        return isServer;

    }

    private String[] getTypeDevice(String ipDevice, List<Community> snmpCommunities) {

        try {

            boolean isDevice = isSwitch(ipDevice, snmpCommunities);
            if (isDevice) {
                return new String[]{null, "switch"};
            }

            isDevice = isPrinter(ipDevice, snmpCommunities);
            if (isDevice) {
                return new String[]{"impressora", "impressora"};
            }

            isDevice = isNobreak(ipDevice, snmpCommunities);
            if (isDevice) {
                return new String[]{"nobreak", "nobreak"};
            }

            isDevice = isServer(ipDevice);
            if (isDevice) {
                return new String[]{null, "servidor"};
            }

            return null;

        } catch (Exception ex) {
            return null;

        }

    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    private String getResponse(CommunityTarget target, Snmp snmp, String oid, String add) throws Exception {

        PDU pdu = new PDU();
        pdu.setType(PDU.GET);
        pdu.add(new VariableBinding(new OID("." + oid + add)));

        ResponseEvent responseEvent = snmp.send(pdu, target);
        PDU response = responseEvent.getResponse();

        for (int i = 0; i < response.size(); i++) {

            VariableBinding variableBinding = response.get(i);

            if (variableBinding.getOid().toString().equalsIgnoreCase(oid + add)) {

                if (variableBinding.isException() || variableBinding.getVariable().isException()
                        || !variableBinding.getOid().isValid() || variableBinding.getOid().isException()) {

                    throw new Exception("no results found for oid " + (oid + add));
                }

                return variableBinding.getVariable().toString();

            }

        }

        throw new Exception("no results found for oid " + (oid + add));

    }

    private String getSysServices(String ipDevice, Community community) {

        String oid = "1.3.6.1.2.1.1.7.0";

        TransportMapping transport = null;
        Snmp snmp = null;

        try {

            transport = new DefaultUdpTransportMapping();
            snmp = new Snmp(transport);
            transport.listen();

            CommunityTarget target = getTarget(ipDevice, community);

            return getResponse(target, snmp, oid, "");

        } catch (Exception ex) {
            return null;

        } finally {
            try {
                snmp.close();
                transport.close();
            } catch (Exception ex) {
            }

        }

    }

    private boolean checkCommunity(String ipDevice, Community community) {

        String oid = "1.3.6.1.2.1.1.7.0";

        TransportMapping transport = null;
        Snmp snmp = null;

        try {

            transport = new DefaultUdpTransportMapping();
            snmp = new Snmp(transport);
            transport.listen();

            CommunityTarget target = getTarget(ipDevice, community);

            return !getResponse(target, snmp, oid, "").isEmpty();

        } catch (Exception ex) {
            return false;

        } finally {
            try {
                snmp.close();
                transport.close();
            } catch (Exception ex) {
            }

        }

    }

    private Community checkCommunities(String ipDevice, List<Community> communities) {

        for (Community c : communities) {
            boolean check = checkCommunity(ipDevice, c);
            if (check) {
                return c;
            }

        }

        return null;
    }

    private String getSysServices(String ipDevice, List<Community> communities) {

        for (Community c : communities) {
            String service = getSysServices(ipDevice, c);
            if (service != null && !service.isEmpty()) {
                return service;
            }

        }

        return null;
    }

    private Map<String, String> getResponsesOidDvice(String ipDevice, Community community, String oid, String add) throws Exception {

        TransportMapping transport = null;
        Snmp snmp = null;
        Map<String, String> map = new HashMap<>();

        try {

            transport = new DefaultUdpTransportMapping();
            snmp = new Snmp(transport);
            transport.listen();

            CommunityTarget target = getTarget(ipDevice, community);

            DefaultPDUFactory defaultPDUFactory = new DefaultPDUFactory(PDU.GETBULK);

            TreeUtils treeUtils = new TreeUtils(snmp, defaultPDUFactory);
            treeUtils.setIgnoreLexicographicOrder(!treeUtils.isIgnoreLexicographicOrder());

            List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid + add));

            for (TreeEvent event : events) {
                if (event == null) {
                    continue;
                }
                if (event.isError()) {
                    continue;
                }
                VariableBinding[] varBindings = event.getVariableBindings();
                for (VariableBinding varBinding : varBindings) {
                    if (varBinding == null) {
                        continue;
                    }
                    map.put(varBinding.getOid().toString(), varBinding.getVariable().toString());
                }
            }

            return map;

        } catch (Exception ex) {
            throw ex;

        } finally {
            try {
                snmp.close();
                transport.close();
            } catch (Exception ex) {
            }

        }

    }

    private String getResponseOidDevice(String ipDevice, List<Community> communities, String oid, String add) throws Exception {

        for (Community community : communities) {

            TransportMapping transport = null;
            Snmp snmp = null;

            try {

                transport = new DefaultUdpTransportMapping();
                snmp = new Snmp(transport);
                transport.listen();

                CommunityTarget target = getTarget(ipDevice, community);

                return getResponse(target, snmp, oid, add);

            } catch (Exception ex) {
            } finally {
                try {
                    snmp.close();
                    transport.close();
                } catch (Exception ex) {
                }

            }

        }

        throw new Exception("no results found for oid " + (oid + add));

    }

    /**
     * Formar o IP com base na OID
     *
     * @param oid
     * @return
     */
    private String getIpByOid(String oid) {
        String ips[] = oid.split("\\.");
        String ipg1 = ips[(ips.length - 1) - 3];
        String ipg2 = ips[(ips.length - 1) - 2];
        String ipg3 = ips[(ips.length - 1) - 1];
        String ipg4 = ips[(ips.length - 1)];
        String ip = ipg1 + "." + ipg2 + "." + ipg3 + "." + ipg4;
        return ip;

    }

    /**
     * Coleta o sistema operacional do dispositivo
     *
     * @param device
     * @param ip
     * @param mac
     */
    private void setOsDevice(Device device, String ip, String mac) {
        try {
            String os = Util.getOsDeviceFromNMap(beatByParams.getPasswordSudo(), ip, mac);
            device.setOs(os);

        } catch (Exception ex) {
            device.setOs(null);
        }
    }

    /**
     * Coleta o fabricante do dispositivo
     *
     * @param device
     * @param mac
     */
    private void setVendorDevice(Device device, String mac) {
        try {
            String vendor = Util.getVendorByMacAdrress(mac);
            device.setVendor(vendor);

        } catch (Exception ex) {
            device.setVendor(null);
        }
    }

    /**
     * Coleta a categoria, sistema e descrição do dispositivo
     *
     * @param device
     * @param mac
     * @param ip
     * @param communities
     */
    private void setTypeDevice(Device device, String mac, String ip, List<Community> communities) {

        String systems = null, category = null, description = null;

        try {

            // pega o cache
            JSONObject cacheTypeDevice = Util.getTypeDevice(mac);

            // se houver cache, resgata os valores
            if (cacheTypeDevice != null) {

                systems = (!cacheTypeDevice.has("systems") || cacheTypeDevice.get("systems") == JSONObject.NULL ? null : cacheTypeDevice.getString("systems"));
                category = (!cacheTypeDevice.has("category") || cacheTypeDevice.get("category") == JSONObject.NULL ? null : cacheTypeDevice.getString("category"));
                description = (!cacheTypeDevice.has("description") || cacheTypeDevice.get("description") == JSONObject.NULL ? null : cacheTypeDevice.getString("description"));

            } else {

                // se não houver cache, busca via snmp
                String type[] = getTypeDevice(ip, communities);

                if (type != null) {

                    systems = type[0];
                    category = type[1];

                    if (category != null && category.equalsIgnoreCase("switch")) {
                        try {
                            description = getResponseOidDevice(ip, communities, "1.3.6.1.2.1.1.1.0", "");
                        } catch (Exception ex) {
                        }
                    }

                }

                Util.saveTypeDevice(mac, category, systems, description);

            }

        } catch (Exception ex) {
        }

        device.setSystems(systems);
        device.setCategory(category);
        device.setDescription(description);

    }

    private Map<String, Device> convertResponseToListDevices(Map<String, String> response,
            Map<String, Device> map, List<Community> communities) {

        Map<String, Device> devices = new HashMap<>();

        for (Map.Entry<String, String> entry : response.entrySet()) {

            String key = entry.getKey();
            String mac = entry.getValue();

            Device device = map.get(mac);
            String ip = getIpByOid(key);

            if (device == null) {

                System.out.println("Checando o dispositivo " + ip);

                device = new Device(null, ip, mac);

                setTypeDevice(device, mac, ip, communities);
                System.out.println("Categoria, sistema e descrição checada do dispositivo " + ip);

                setVendorDevice(device, mac);
                System.out.println("Fabricante checado do dispositivo " + ip);

                setOsDevice(device, ip, mac);
                System.out.println("Sistema operacional checado do dispositivo " + ip);

            } else {
                System.out.println("Dispositivo " + ip + "já veficado anteriormente");

            }

            devices.put(mac, device);

        }

        return devices;
    }

    private Map<String, Device> getNetworkDevicesByTableARP(
            String ipDevice,
            List<Community> communities,
            Map<String, Device> map,
            List<String> history) {

        if (history.contains(ipDevice)) {
            System.out.println("IP: " + ipDevice + " já passou por uma verificação");
            return map;
        }
        history.add(ipDevice);

        Community c = checkCommunities(ipDevice, communities);
        if (c == null) {
            System.out.println("IP: " + ipDevice + " não possui o protocolo SNMP");
            return map;
        }

        Map<String, Device> dev;
        try {
            dev = convertResponseToListDevices(getResponsesOidDvice(ipDevice, c, "1.3.6.1.2.1.4.22.1.2", ""), map, communities);
        } catch (Exception ex) {
            return map;
        }

        System.out.println("IP: " + ipDevice + " possui a tabela ARP");

        map.putAll(dev);

        for (Map.Entry<String, Device> entry : dev.entrySet()) {
            if (!entry.getValue().isSwitch()) {
                System.out.println("IP: " + entry.getValue().getIp() + " não é um switch");
                continue;
            }
            getNetworkDevicesByTableARP(entry.getValue().getIp(), communities, map, history);
        }

        return map;
    }

    public List<Switch> getListSwitchs(Map<String, Device> devices) {
        List<Switch> switchs = new ArrayList<>();
        for (Map.Entry<String, Device> entry : devices.entrySet()) {
            if (entry.getValue().isSwitch()) {
                switchs.add(new Switch(entry.getValue()));
            }
        }
        return switchs;

    }

    private String getNewKey(String key) {
        String oids[] = key.split("\\.");
        List<String> newOids = new ArrayList<>();
        for (int i = 11; i < oids.length; i++) {
            newOids.add(oids[i]);
        }
        String newKey = String.join(".", newOids);
        return newKey;

    }

    public Map<String, String> renameKeys(Map<String, String> map) {
        Map<String, String> newMap = new HashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String newKey = getNewKey(entry.getKey());
            String value = entry.getValue();
            newMap.put(newKey, value);

        }

        return newMap;

    }

    public void addConnectedDevices(Switch swt, Map<String, Device> networkDevicesByTableARP,
            List<Community> communities, Map<String, Device> connectedDeviceOnline) {

        Community community = checkCommunities(swt.getIp(), communities);

        if (community == null) {
            return;
        }

        try {

            // Retorna o mac dos dispositivos conectados no switch (oid, mac)
            Map<String, String> address = renameKeys(getResponsesOidDvice(swt.getIp(), community, "1.3.6.1.2.1.17.4.3.1.1", ""));

            // Retorna a porta onde os dispositivos estão conectados (oid, port)
            Map<String, String> ports = renameKeys(getResponsesOidDvice(swt.getIp(), community, "1.3.6.1.2.1.17.4.3.1.2", ""));

            for (Map.Entry<String, String> entry : ports.entrySet()) {

                String key = entry.getKey(); // oid
                Integer port = Integer.parseInt(entry.getValue()); // port                

                String mac = address.get(key); // mac                
                Device device = networkDevicesByTableARP.get(mac); // device

                if (device != null) {
                    connectedDeviceOnline.put(mac, device);
                    swt.addDevice(port, device);
                }

            }

        } catch (Exception ex) {
        }

    }

    /**
     * Retorna verdadeiro caso a porta em que um dispositivo está conectado
     * contenha mais dispositivo conectados do que em outra porta em que o mesmo
     * dispositivo esteja conectado
     *
     * @param deviceCheck
     * @param switchChek
     * @param portCheck
     * @param list
     * @return
     */
    public boolean checkIfDeviceIsDuplicated(Device deviceCheck, Switch switchChek,
            Port portCheck, List<Switch> list) {

        for (Switch swt : list) {

            if (swt.getIp().equalsIgnoreCase(switchChek.getIp())) {
                continue;
            }

            Port port = swt.containsDevice(deviceCheck);

            if (port == null) {
                continue;
            }

            return portCheck.getDevices().size() > port.getDevices().size();

        }

        return false;

    }

    /**
     * Remove dispositivos duplicados em outras portas de outros switchs
     *
     * @param list
     */
    public void cleanDuplicateDevices(List<Switch> list) {

        for (Switch swt : list) {

            for (Port port : swt.getPorts()) {

                Iterator<Device> devices = port.getDevices().iterator();

                while (devices.hasNext()) {

                    Device dev = devices.next();

                    // é um switch
                    if (dev.isSwitch()) {
                        continue;
                    }

                    // não é um switch
                    boolean deviceIsDuplicated = checkIfDeviceIsDuplicated(dev, swt, port, list);

                    if (deviceIsDuplicated) {
                        devices.remove();
                    }

                }

            }

        }

    }

    public void addJSONObjectDevice(Device device, Port port, JSONArray devicesJSON) throws JSONException {

        if (port == null) {

            JSONObject obj = new JSONObject();

            // objeto device
            JSONObject deviceJson = new JSONObject();
            deviceJson.put("mac", device.getMac());
            deviceJson.put("vendor", device.getVendor() == null ? JSONObject.NULL : device.getVendor());
            deviceJson.put("os_details", device.getOs() == null ? JSONObject.NULL : device.getOs());
            deviceJson.put("ip", device.getIp());
            if (device.getCategory() != null && !device.getCategory().isEmpty()) {
                deviceJson.put("category", device.getCategory());
            }
            if (device.getSystems() != null && !device.getSystems().isEmpty()) {
                JSONArray systemsArrayJson = new JSONArray();
                systemsArrayJson.put(device.getSystems());
                deviceJson.put("systems", systemsArrayJson);
            }
            deviceJson.put("type", "");
            obj.put("device", deviceJson);

            // objeto hosts
            JSONArray hostArrayJson = new JSONArray();
            obj.put("host", hostArrayJson);

            // objeto ports
            JSONArray portsArrayJson = new JSONArray();
            JSONObject portJson = new JSONObject();
            portJson.put("port", portsArrayJson);
            obj.put("ports", portJson);

            obj.put("id_doc", "device-" + device.getMac());
            
            devicesJSON.put(obj);

            return;
        }

        for (int i = 0; i < devicesJSON.length(); i++) {

            JSONObject obj = devicesJSON.getJSONObject(i);

            if (!obj.getJSONObject("device").getString("mac").equalsIgnoreCase(device.getMac())) {
                continue;
            }

            obj.getJSONArray("host").put(port.getSwitch().getIp());
            obj.getJSONObject("ports").getJSONArray("port").put(port.getPort());

            return;

        }

        JSONObject obj = new JSONObject();

        // objeto device
        JSONObject deviceJson = new JSONObject();
        deviceJson.put("mac", device.getMac());
        deviceJson.put("vendor", device.getVendor() == null ? JSONObject.NULL : device.getVendor());
        deviceJson.put("os_details", device.getOs() == null ? JSONObject.NULL : device.getOs());
        deviceJson.put("ip", device.getIp());
        if (device.getCategory() != null && !device.getCategory().isEmpty()) {
            deviceJson.put("category", device.getCategory());
        }
        if (device.getSystems() != null && !device.getSystems().isEmpty()) {
            JSONArray systemsArrayJson = new JSONArray();
            systemsArrayJson.put(device.getSystems());
            deviceJson.put("systems", systemsArrayJson);
        }
        deviceJson.put("type", "");
        obj.put("device", deviceJson);

        // objeto hosts
        JSONArray hostArrayJson = new JSONArray();
        hostArrayJson.put(port.getSwitch().getIp());
        obj.put("host", hostArrayJson);

        // objeto ports
        JSONArray portsArrayJson = new JSONArray();
        portsArrayJson.put(port.getPort());
        JSONObject portJson = new JSONObject();
        portJson.put("port", portsArrayJson);
        portJson.put("type", 1);
        obj.put("ports", portJson);

        obj.put("id_doc", "device-" + device.getMac());
        
        devicesJSON.put(obj);

    }

    @Override
    public void run() {

        while (run) {

            System.out.println("Starting the queries ...");

            // Busca todos os dispositivos conectados na rede
            // Busca recursiva, por meio da tabela ARP
            Map<String, Device> networkDevicesByTableARP = new HashMap<>();
            for (Switch swt : switchsByParams) {
                getNetworkDevicesByTableARP(
                        swt.getIp(),
                        communitiesbyParams,
                        networkDevicesByTableARP,
                        new ArrayList<>()
                );
            }

            // Busca todos os switchs da rede e os dispositivos conectado nele
            List<Switch> switchs = getListSwitchs(networkDevicesByTableARP);
            Map<String, Device> connectedDeviceOnline = new HashMap<>();
            for (Switch swt : switchs) {
                addConnectedDevices(
                        swt,
                        networkDevicesByTableARP,
                        communitiesbyParams,
                        connectedDeviceOnline
                );
            }
            cleanDuplicateDevices(switchs);

            // Prepara json            
            JSONArray devicesJSON = new JSONArray();
            for (Map.Entry<String, Device> entry : connectedDeviceOnline.entrySet()) {

                Device device = entry.getValue();

                boolean flag = false;

                for (Switch swt : switchs) {
                    Port port = swt.containsDevice(device);
                    if (port == null) {
                        continue;
                    }
                    try {
                        addJSONObjectDevice(device, port, devicesJSON);
                    } catch (Exception ex) {
                    }
                    flag = true;
                }

                if (!flag) {
                    try {
                        addJSONObjectDevice(device, null, devicesJSON);
                    } catch (JSONException ex) {
                    }
                }

            }
//
//            // Lista dispositivos
//            for (Switch swt : switchs) {
//                System.out.println("");
//                System.out.println("Switch: " + swt.getIp());
//
//                for (Port port : swt.getPorts()) {
//                    System.out.println("");
//                    System.out.println("\t\tPorta: " + port.getPort());
//
//                    for (Device dev : port.getDevices()) {
//                        //System.out.println("\t\t\tDispositivo: " + dev.getIp() + " / " + dev.getMac() + " / " + dev.getCategory());
//                        System.out.println("\t\t\tDispositivo: " + dev.toString());
//                    }
//
//                }
//
//                System.out.println("");
//                System.out.println("==================================================================================");
//
//            }

            // Seta outros atributos, atributos que qualquer documento tem
            JSONObject jsonDefault = new JSONObject();

            try {

                jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                JSONObject metadataJSON = new JSONObject();
                metadataJSON.put("beat", beatByParams.getName());
                metadataJSON.put("version", beatByParams.getVersion());
                jsonDefault.put("@metadata", metadataJSON);

                JSONObject beatJSON = new JSONObject();
                beatJSON.put("name", beatByParams.getName());
                beatJSON.put("version", beatByParams.getVersion());
                jsonDefault.put("beat", beatJSON);

                jsonDefault.put("tags", beatByParams.getTags());

            } catch (JSONException ex) {
            }

            JSONArray jsonSubmit = new JSONArray();

            for (int i = 0; i < devicesJSON.length(); i++) {
                try {
                    jsonSubmit.put(getJson(jsonDefault, devicesJSON.getJSONObject(i)));
                } catch (JSONException ex) {
                }
            }

            System.out.println("");
            System.out.println("Data: " + jsonSubmit.toString());

            try {

                for (Output out : listOutputByParams) {
                    
                    for (int i = 0; i < jsonSubmit.length(); i++) {
                        
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonSubmit.getJSONObject(i).toString());
                            os.flush();
                        }                        
                        
                    }

                }

            } catch (Exception ex) {
            }

            // Intervalo de espera
            try {
                Thread.sleep(beatByParams.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
