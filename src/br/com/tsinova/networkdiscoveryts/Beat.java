package br.com.tsinova.networkdiscoveryts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Beat {
    
    private String name;
    private String version;
    private JSONArray tags;  
    private String passwordSudo;
    private int interval;
    
    public Beat() {
    }
    
    public Beat(JSONObject obj) throws JSONException{        
        name = obj.getString("name");
        version = obj.getString("version");
        tags = obj.getJSONArray("tags");
        passwordSudo = obj.getString("password_sudo");
        interval = obj.getInt("interval");
        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public JSONArray getTags() {
        return tags;
    }

    public void setTags(JSONArray tags) {
        this.tags = tags;
    }

    public String getPasswordSudo() {
        return passwordSudo;
    }

    public void setPasswordSudo(String passwordSudo) {
        this.passwordSudo = passwordSudo;
    }    

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }    
    
}
