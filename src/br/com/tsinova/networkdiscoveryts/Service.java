package br.com.tsinova.networkdiscoveryts;

import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Service extends Thread {
    
    private final List<ReadSend> listReadSends;

    public Service() {
        listReadSends = new ArrayList<>();
    }

    public List<Community> getListCommunities(JSONObject json) throws Exception {
        List<Community> listCommunities = new ArrayList<>();
        JSONArray arrayCommunities = json.getJSONArray("snmp_communities");
        for (int i = 0; i < arrayCommunities.length(); i++) {
            JSONObject community = arrayCommunities.getJSONObject(i);
            listCommunities.add(new Community(community));

        }
        return listCommunities;

    }

    public List<Switch> getListSwitches(JSONObject json) throws Exception {
        List<Switch> listSwitchs = new ArrayList<>();
        JSONArray arraySwitchs = json.getJSONArray("switchs");
        for (int i = 0; i < arraySwitchs.length(); i++) {
            String ip = arraySwitchs.getString(i);
            listSwitchs.add(new Switch(ip));
        }
        return listSwitchs;

    }

    private Beat getBeat(JSONObject json) throws Exception {
        JSONObject beatJSON = json.getJSONObject("beat");
        Beat beat = new Beat(beatJSON);
        return beat;
        
    }

    public List<Output> getListOutputs(JSONObject json) throws Exception {
        JSONObject outputJSON = json.getJSONObject("output");
        List<Output> listOutputs = new ArrayList<>();
        if (outputJSON.has("logstash")) {
            OutputLogstash outputLogstash = new OutputLogstash();
            outputLogstash.setHost(outputJSON.getJSONObject("logstash").getString("host"));
            outputLogstash.setPort(outputJSON.getJSONObject("logstash").getInt("port"));
            listOutputs.add(outputLogstash);
        }

        return listOutputs;

    }

    @Override
    public void run() {
        
        Util.setConfigUnirest();

        // Efetua a leitura dos parâmetros ...
        System.out.println("Reading parameters ...");

        List<Switch> listSwitchs;
        List<Output> listOutputs;
        List<Community> listCommunities;
        Beat beat;

        try {

            JSONObject json = new JSONObject(new JSONTokener(new FileInputStream(Paths.get("networkdiscoveryts.json").toFile())));

            listCommunities = getListCommunities(json);
            listSwitchs = getListSwitches(json);
            listOutputs = getListOutputs(json);
            beat = getBeat(json);

            System.out.println("Successful reading");

        } catch (Exception ex) {
            System.err.println("Failed to read");
            ex.printStackTrace();
            return;

        }

        // inicia processos
        System.out.println("Starting reading and sending processes ...");

        ReadSend readSend = new ReadSend(listSwitchs, listCommunities, listOutputs, beat);
        readSend.start();
        listReadSends.add(readSend);

        System.out.println("Read and send processes successfully started");

    }

    public static void main(String[] args) {       

        Service service = new Service();
        service.start();

    }

}
